/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function promptPersonDetails(){
        let fullName = prompt("Enter your full name: ");
		let age = prompt("Enter your age: ");
		let location= prompt("Enter your location: ");

		console.log("Hello, "+fullName);
		console.log("You are "+age+" years old.");
		console.log("You live in "+location+".");
    };
	promptPersonDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function myFavoriteBands(){
        let bandOne = "Mayday Parade";
		let bandTwo = "Mayonnaise";
		let bandThree= "Parokya ni Edgar";
		let bandFour= "Spongecola";
		let bandFive= "Secondhand Serenade";

		console.log("Your top 5 favorite bands are: ");
		console.log("1. "+bandOne);
		console.log("2. "+bandTwo);
		console.log("3. "+bandThree);
		console.log("4. "+bandFour);
		console.log("5. "+bandFive);
    };
	myFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myFavoriteMovies(){
        let movieOne = "YOUR NAME";
		let movieOneRating= "98%";
		let movieTwo ="HARRY POTTER AND THE DEATHLY HALLOWS: PART 2" ;
		let movieTwoRating ="96%";
		let movieThree = "MISSION: IMPOSSIBLE -- GHOST PROTOCOL" ;
		let movieThreeRating="93%";
		let movieFour = "AVATAR";
		let movieFourRating ="82%";
		let movieFive = "The Dark Knight ";
		let movieFiveRating ="94%";

		console.log("Your top 5 favorite movies are: ");
		console.log("1. "+movieOne);
		console.log("Rotten Tomatoes Rating: "+movieOneRating);
		console.log("2. "+movieTwo);
		console.log("Rotten Tomatoes Rating: "+movieTwoRating);
		console.log("3. "+movieThree);
		console.log("Rotten Tomatoes Rating: "+movieThreeRating);
		console.log("4. "+movieFour);
		console.log("Rotten Tomatoes Rating: "+movieFourRating);
		console.log("5. "+movieFive);
		console.log("Rotten Tomatoes Rating: "+movieFiveRating);
    };
	myFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/* console.log(friend1);
console.log(friend2); */